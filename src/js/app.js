"use strict";

import * as flsFunctions from "./modules/functions.js";

flsFunctions.isWebp();

const hamMenu = document.querySelector(".ham-wrapper");
const hamContent = document.querySelector(".ham-nav__wrapper");

const lineUpper = document.querySelector(".ham-menu__line-upper");
const lineMiddle = document.querySelector(".ham-menu__line-middle");
const lineLower = document.querySelector(".ham-menu__line-lower");

hamMenu.addEventListener("click", (e) => {
  const value = e.target;
  if (!!value) {
    hamContent.classList.toggle("show");
    lineUpper.classList.toggle("active");
    lineMiddle.classList.toggle("active");
    lineLower.classList.toggle("active");
  }
});

document.addEventListener("click", (e) => {
  const value = e.target.parentElement;
  if (value.classList.contains("header__container")) {
    hamContent.classList.remove("show");
    lineUpper.classList.remove("active");
    lineMiddle.classList.remove("active");
    lineLower.classList.remove("active");
  }
});
