# Step Project Forkio

  If you encounter an error while building gulp, then you need to manually install the following plugin:
  npm install webp-converter@2.2.3 --save-dev

  https://cherrych.gitlab.io/step-project-forkio/
	
# Technologies used in this project:
- HTML
- CSS
- SCSS
- JS
- Gulp
- Gitlab

# Composition of project participants:

- Artur Cherychenko
- Sergey Kabanets

# Task.

- Artur Cherychenko:

Creating a remote repository.
Layout of the site header with the top menu (including the drop-down menu at low screen resolution).
Layout of the People Are Talking About Fork section.

- Sergey Kabanets:

Creating the gulpfile.js file.
Layout of the Revolutionary Editor block.
Layout of the Here is what you get section.
Layout of the Fork Subscription Pricing section.
